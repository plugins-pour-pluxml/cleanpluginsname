<?php 
	$LANG = array(
		'L_TITLE'=>'cleanPluginName',
		'L_DESCRIPTION'=>'Ce plugin permet de nettoyer le nom des dossiers des nouveaux plugins t&eacutel&eacutecharg&eacutes sur une plateforme utilisant git (github gitlab...) afin de supprimer la cha&icircne &quot-master&quot ajout&eacutee &agrave la fin de ces noms.',
		'L_NO'=>'Non',
		'L_YES'=>'Oui',
        'L_CLEANING_INTRO'=>'La chaîne de caractères "-master" a été trouvée dans le nom du dossier de certains plugins.',
        'L_PLUGINS_CLEAN'=> 'Les plugins suivants ont pu être nettoyés : ',
        'L_PLUGINS_NOT_DETECTED'=>'Les plugins suivants n\'ont pas pu être nettoyés. Vous devez les nettoyer manuellement : ', 
#MORE
	);