<?php
/**
* Plugin cleanPluginName
*
* @package	PLX
* @version	1.0
* @date	03/02/19
* @author Cyril MAGUIRE
**/
class cleanPluginName extends plxPlugin {

	public function __construct($default_lang) {
		# appel du constructeur de la classe plxPlugin (obligatoire)
		parent::__construct($default_lang);

		# Declaration d'un hook (existant ou nouveau)
		$this->addHook('AdminSettingsPluginsTop','AdminSettingsPluginsTop');
	}

	# Activation / desactivation
	public function OnActivate() {
		# code à executer à l’activation du plugin
	}
	public function OnDeactivate() {
		# code à executer à la désactivation du plugin
	}

	public function listADir(string $startpath, bool $onlydir=false, array $exclude=array(), bool $order = false):array
	{
	    if (!is_array($exclude)) $exclude = array();
	    $ritit = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($startpath), \RecursiveIteratorIterator::CHILD_FIRST); 
	    $r = array(); 
	    foreach ($ritit as $splFileInfo) {
	        if ($splFileInfo->getFilename() != '.' && $splFileInfo->getFilename() != '..' && !in_array($splFileInfo->getFilename(), $exclude)) {
	            $path = $splFileInfo->isDir()
	            ? array($splFileInfo->getFilename() => array()) 
	            : array($splFileInfo->getFilename()); 
	            for ($depth = $ritit->getDepth() - 1; $depth >= 0; $depth--) { 
	                $path = array($ritit->getSubIterator($depth)->current()->getFilename() => $path);                     
	            }
	            $r = array_merge_recursive($r, $path);
	        }
	        unset($path);
	    } 
	    if ($onlydir) {
	        foreach ($r as $key => $path) {
	            if (!is_array($path)) {
	                unset($r[$key]);
	            }
	        }
	    }
	    foreach ($exclude as $key => $value) {
	        if (isset($r[$value])) {
	            unset($r[$value]);
	        }
	    }
	    if ($order) {
	        $r = $this->order($r);
	    }
	    return $r;
	}

	public function order($array)
	{
		$tmp = [];
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                $tmp[$k] = $v;
                unset($array[$k]);
            }
        }
        natsort($array);
        ksort($tmp);
        $array = $tmp + $array;
        return $array;
	}
	

	########################################
	# HOOKS
	########################################


	########################################
	# AdminSettingsPluginsTop
	########################################
	# Description:
	public function AdminSettingsPluginsTop(){
		$plxAdmin = plxAdmin::getInstance();

		$plugDir = realpath(PLX_PLUGINS);
		$allPlugs = array_values($this->order(array_merge(array_keys($plxAdmin->plxPlugins->aPlugins),array_keys($plxAdmin->plxPlugins->getInactivePlugins()))));
		$dirs = array_keys($this->listADir($plugDir,true,array(),true));
		$notInList = array_diff($dirs, $allPlugs);
		$msgCPN = '';
		foreach ($notInList as $key => $value) {
			if ($value != 'cache') {
				if (strpos($value,'-master') !== false) {
					$msgCPN = $this->getLang('L_CLEANING_INTRO').'<br/>';
					$cleanName = substr($value,0,-7);
					if (!rename($plugDir.DIRECTORY_SEPARATOR.$value, $plugDir.DIRECTORY_SEPARATOR.$cleanName) ) {
						$cleanPluginNameErrors[] = $value;
					} else {
						$cleanPluginNameDone[] = $cleanName;
					}
				}
			}
		}
		
		if (isset($cleanPluginNameErrors)) {
			$msgCPN .= '<span style="color:red;">'.$this->getLang('L_PLUGINS_NOT_DETECTED');
			foreach ($cleanPluginNameErrors as $key => $cPNe) {
				$msgCPN .= $cPNe.', ';
			}
			$msgCPN = substr($msgCPN,0,-2).'.</span>';
			$msgCPN .= '<br/>';
		}
		if (isset($cleanPluginNameDone)) {
			$msgCPN .= $this->getLang('L_PLUGINS_CLEAN');
			foreach ($cleanPluginNameDone as $key => $cPNd) {
				$msgCPN .= $cPNd.', ';
			}
			$msgCPN = substr($msgCPN,0,-2).'.';
		}		
		if ($msgCPN != '') {
			echo '<div class="info">'.$msgCPN.'</div>';
		}
	}
}





/* Pense-bete:
 * Récuperer des etres du fichier eters.xml
 *	$this->getParam("<nom du etre>")
 *	$this-> setParam ("1", 12345, "numeric")
 *	$this->saveParams()
 *
 *	plxUtils::strCheck($string) : sanitize string
 *
 * 
 * Quelques constantes utiles: 
 * PLX_CORE
 * PLX_ROOT
 * PLX_CHARSET
 * PLX_PLUGINS
 * PLX_CONFIG_PATH
 * PLX_ADMIN (true si on est dans admin)
 * PLX_CHARSET
 * PLX_VERSION
 * PLX_FEED
 *
 * Appel de HOOK dans un thème
 *	eval($plxShow->callHook("ThemeEndHead","1"))  ou eval($plxShow->callHook("ThemeEndHead",array("1","2")))
 *	ou $retour=$plxShow->callHook("ThemeEndHead","1"));
 */